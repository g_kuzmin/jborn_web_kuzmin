package ru.kuzmin.api.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kuzmin.MockSecurityConfiguration;
import ru.kuzmin.api.converter.AccountDtoToResponseConverter;
import ru.kuzmin.config.SecurityConfiguration;
import ru.kuzmin.service.AccountDTO;
import ru.kuzmin.service.AccountService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AccountController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class AccountControllerTest {
    @Autowired MockMvc mockMvc;
    @MockBean AccountService accountService;
    @SpyBean AccountDtoToResponseConverter converter;

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void getAccounts() throws Exception {
        List<AccountDTO> accounts = new ArrayList<>();
        accounts.add(new AccountDTO(1L, "1", 1, 1L));
        when(accountService.findAll(1L)).thenReturn(accounts);

        mockMvc.perform(get("/api/display-accounts"))
                .andExpect(status().isOk())
                .andExpect(content().json("[\n" +
                        "  {\n" +
                        "    \"id\": 1,\n" +
                        "    \"name\": \"1\",\n" +
                        "    \"balance\": 1,\n" +
                        "    \"consumerId\": 1\n" +
                        "  }" +
                        "\n" +
                        "]"));
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void addAccount() throws Exception {
        when(accountService.create(anyLong(), anyString(), anyLong()))
                .thenReturn(new AccountDTO(1L, "1", 1, 1L));

        mockMvc.perform(post("/api/add-account")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"name\": \"1\",\n" +
                                "  \"balance\": 1\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "    \"id\": 1,\n" +
                        "    \"name\": \"1\",\n" +
                        "    \"balance\": 1,\n" +
                        "    \"consumerId\": 1\n" +
                        "}"));
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void deleteAccount() throws Exception {
        mockMvc.perform(post("/api/delete-account")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"id\": 1\n" +
                                "}"))
                .andExpect(status().isOk());
    }
}
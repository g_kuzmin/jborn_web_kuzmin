package ru.kuzmin.api.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kuzmin.MockSecurityConfiguration;
import ru.kuzmin.api.converter.TransactionDtoToResponseConverter;
import ru.kuzmin.config.SecurityConfiguration;
import ru.kuzmin.service.TransactionDTO;
import ru.kuzmin.service.TransactionService;

import java.sql.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TransactionController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class TransactionControllerTest {
    @Autowired MockMvc mockMvc;
    @MockBean TransactionService transactionService;
    @SpyBean TransactionDtoToResponseConverter converter;

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void addTransaction() throws Exception {
        when(transactionService.create(anyLong(), anyLong(), anyLong(), anyLong(), any(Date.class), any()))
                .thenReturn(new TransactionDTO(1L, 1L, 2L, 1, new Date(2023 - 1900, 2 - 1, 3)));

        mockMvc.perform(post("/api/add-transaction")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"fromAccountId\":0,\n" +
                                "  \"toAccountId\":1,\n" +
                                "  \"amount\":300,\n" +
                                "  \"categories\":[\n" +
                                "    1\n" +
                                "  ]\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"id\": 1,\n" +
                        "  \"fromAccountId\": 1,\n" +
                        "  \"toAccountId\": 2,\n" +
                        "  \"amount\": 1,\n" +
                        "  \"createdDate\": \"2023-02-03\"\n" +
                        "}"));
    }
}
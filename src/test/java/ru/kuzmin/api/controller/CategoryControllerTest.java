package ru.kuzmin.api.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kuzmin.MockSecurityConfiguration;
import ru.kuzmin.api.converter.CategoryDtoToResponseConverter;
import ru.kuzmin.api.converter.CategoryReportDtoToResponseConverter;
import ru.kuzmin.config.SecurityConfiguration;
import ru.kuzmin.service.CategoryDTO;
import ru.kuzmin.service.CategoryReportDTO;
import ru.kuzmin.service.CategoryService;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CategoryController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class CategoryControllerTest {
    @Autowired MockMvc mockMvc;
    @MockBean CategoryService categoryService;
    @SpyBean CategoryDtoToResponseConverter dtoConverter;
    @SpyBean CategoryReportDtoToResponseConverter reportConverter;

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void getCategories() throws Exception {
        List<CategoryDTO> categories = new ArrayList<>();
        categories.add(new CategoryDTO(1L, 1L, "1"));
        when(categoryService.findAll(1L)).thenReturn(categories);

        mockMvc.perform(get("/api/display-categories"))
                .andExpect(status().isOk())
                .andExpect(content().json("[\n" +
                        "  {\n" +
                        "    \"id\": 1,\n" +
                        "    \"consumerId\": 1,\n" +
                        "    \"name\": \"1\"\n" +
                        "  }\n" +
                        "]"));
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void addCategory() throws Exception {
        when(categoryService.create(anyLong(), anyString()))
                .thenReturn(new CategoryDTO(1L, 1L, "1"));

        mockMvc.perform(post("/api/add-category")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"name\": \"1\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "    \"id\": 1,\n" +
                        "    \"name\": \"1\"" +
                        "\n" +
                        "}"));
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void updateCategory() throws Exception {
        when(categoryService.update(anyLong(), anyLong(), anyString()))
                .thenReturn(new CategoryDTO(1L, 1L, "2"));

        mockMvc.perform(post("/api/update-category")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"id\": 1,\n" +
                                "    \"name\": \"2\"" +
                                "\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "    \"id\": 1,\n" +
                        "    \"name\": \"2\"" +
                        "\n" +
                        "}"));

    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void deleteCategory() throws Exception {
        mockMvc.perform(post("/api/delete-category")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"id\": 1\n" +
                                "}"))
                .andExpect(status().isOk());
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void incomeReport() throws Exception {
        List<CategoryReportDTO> reports = new ArrayList<>();
        reports.add(new CategoryReportDTO("1", 1));
        when(categoryService.incomeReport(anyLong(), any(Date.class), any(Date.class)))
                .thenReturn(reports);

        mockMvc.perform(post("/api/income")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"beginDate\": \"2000-04-23T18:25:43.511Z\",\n" +
                                "  \"endDate\": \"2022-12-23T18:25:43.511Z\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("[\n" +
                        "  {\n" +
                        "    \"name\": \"1\",\n" +
                        "    \"sum\": 1\n" +
                        "  }\n" +
                        "]"));
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void outcomeReport() throws Exception {
        List<CategoryReportDTO> reports = new ArrayList<>();
        reports.add(new CategoryReportDTO("1", 1));
        when(categoryService.outcomeReport(anyLong(), any(Date.class), any(Date.class)))
                .thenReturn(reports);

        mockMvc.perform(post("/api/outcome")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"beginDate\": \"2000-04-23T18:25:43.511Z\",\n" +
                                "  \"endDate\": \"2022-12-23T18:25:43.511Z\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("[\n" +
                        "  {\n" +
                        "    \"name\": \"1\",\n" +
                        "    \"sum\": 1\n" +
                        "  }\n" +
                        "]"));
    }
}
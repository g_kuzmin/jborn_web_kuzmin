package ru.kuzmin.api.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kuzmin.MockSecurityConfiguration;
import ru.kuzmin.api.converter.UserDtoToResponseConverter;
import ru.kuzmin.config.SecurityConfiguration;
import ru.kuzmin.entity.User;
import ru.kuzmin.service.AuthService;
import ru.kuzmin.service.UserDTO;

import static java.util.Collections.singleton;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.kuzmin.security.UserRole.USER;

@WebMvcTest(AuthController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class AuthControllerTest {
    @Autowired MockMvc mockMvc;
    @MockBean AuthService authService;
    @SpyBean UserDtoToResponseConverter converter;

    @Before
    public void setUp() {
        when(authService.getCurrentUser(1L)).thenReturn(
                new User().setId(1L)
                        .setEmail("123@mail.ru")
                        .setPassword("123")
                        .setRoles(singleton(USER))
        );
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void register() throws Exception {
        when(authService.registration(anyString(), anyString())).thenReturn(new UserDTO(1L, "123@mail.ru"));
        mockMvc.perform(post("/api/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"email\": \"123@mail.ru\",\n" +
                                "  \"password\": \"123\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "\"id\": 1,\n" +
                        "  \"email\": \"123@mail.ru\"\n" +
                        "}"));
    }
}
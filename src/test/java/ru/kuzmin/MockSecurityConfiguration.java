package ru.kuzmin;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.kuzmin.security.CustomGrantedAuthority;
import ru.kuzmin.security.CustomUserDetails;

import static java.util.Collections.singleton;
import static ru.kuzmin.security.UserRole.USER;

@Configuration
public class MockSecurityConfiguration {
    @Bean
    public UserDetailsService userDetailsService() {
        return username -> new CustomUserDetails(
                1L,
                "123@mail.ru",
                "123",
                singleton(new CustomGrantedAuthority(USER))
        );
    }
}

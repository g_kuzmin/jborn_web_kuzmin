package ru.kuzmin.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.kuzmin.api.converter.AccountEntityToAccountDtoConverter;
import ru.kuzmin.entity.Account;
import ru.kuzmin.entity.User;
import ru.kuzmin.exception.CustomException;
import ru.kuzmin.repository.AccountRepository;
import ru.kuzmin.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.singleton;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static ru.kuzmin.security.UserRole.USER;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    @InjectMocks AccountService subj;
    @Mock UserRepository userRepository;
    @Mock AccountRepository accountRepository;
    @Mock AccountEntityToAccountDtoConverter accountDtoConverter;

    @Test
    public void findAll_accountsFound() {
        List<Account> accountEntities = new ArrayList<>();
        User user = userRepository.findById(1L).orElse(null);
        Account account = new Account(1L, "name", 1000, user);
        accountEntities.add(account);
        when(accountRepository.findAccountsByUserId(1)).thenReturn(accountEntities);

        List<AccountDTO> accountDTOSExpected = new ArrayList<>();
        AccountDTO accountExpected = new AccountDTO(1, "name", 1000, 1);
        accountDTOSExpected.add(accountExpected);
        when(accountDtoConverter.convert(account)).thenReturn(accountExpected);

        List<AccountDTO> accountDTOSProvided = subj.findAll(1);
        AccountDTO accountProvided = accountDTOSProvided.get(0);

        assertEquals(accountDTOSExpected, accountDTOSProvided);
        assertEquals(accountExpected, accountProvided);

        verify(accountRepository, times(1)).findAccountsByUserId(1);
        verify(accountDtoConverter, times(1)).convert(account);
    }

    @Test
    public void findAll_accountsNotFound() {
        List<Account> accountEntities = new ArrayList<>();
        when(accountRepository.findAccountsByUserId(1)).thenReturn(accountEntities);

        List<AccountDTO> accountDTOSExpected = new ArrayList<>();
        List<AccountDTO> accountDTOSProvided = subj.findAll(1);

        assertEquals(accountDTOSExpected, accountDTOSProvided);
        assertTrue(accountDTOSProvided.isEmpty());

        verify(accountRepository, times(1)).findAccountsByUserId(1);
        verifyNoInteractions(accountDtoConverter);
    }

    @Test
    public void create_accountInserted() {
        when(userRepository.findById(1L)).thenReturn(
                Optional.ofNullable(new User().setId(1L)
                        .setEmail("123@mail.ru")
                        .setPassword("123")
                        .setRoles(singleton(USER)))
        );

        User user = userRepository.findById(1L).orElse(null);
        Account account = new Account(1L, "name", 1000, user);
        Account accountSaved = new Account("name", 1000, user);
        when(accountRepository.save(accountSaved)).thenReturn(account);

        AccountDTO accountExpected = new AccountDTO(1, "name", 1000, 1);
        when(accountDtoConverter.convert(account)).thenReturn(accountExpected);

        AccountDTO accountProvided = subj.create(1L, "name", 1000);

        assertEquals(accountExpected, accountProvided);

        verify(accountRepository, times(1)).save(accountSaved);
        verify(accountDtoConverter, times(1)).convert(account);
    }

    @Test (expected = CustomException.class)
    public void create_accountNotInserted() {
        User user = userRepository.findById(1L).orElse(null);
        Account account = new Account("name", 1000, user);

        AccountDTO accountDTO = subj.create(1, "name", 1000);

        verify(accountRepository, times(1)).save(account);
        verifyNoInteractions(accountDtoConverter);
    }
}
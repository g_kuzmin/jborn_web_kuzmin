package ru.kuzmin.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.kuzmin.api.converter.TransactionEntityToTransactionDtoConverter;
import ru.kuzmin.entity.Account;
import ru.kuzmin.entity.Category;
import ru.kuzmin.entity.Transaction;
import ru.kuzmin.entity.User;
import ru.kuzmin.exception.CustomException;
import ru.kuzmin.repository.AccountRepository;
import ru.kuzmin.repository.CategoryRepository;
import ru.kuzmin.repository.TransactionRepository;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static java.util.Optional.of;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {
    @InjectMocks TransactionService subj;
    @Mock TransactionRepository transactionRepository;
    @Mock CategoryRepository categoryRepository;
    @Mock AccountRepository accountRepository;
    @Mock TransactionEntityToTransactionDtoConverter transactionDtoConverter;

    @Test
    public void create_inserted() {
        when(categoryRepository.getReferenceById(2L)).thenReturn(new Category(2L, new User().setId(1L), "salary"));
        when(categoryRepository.findById(2L)).thenReturn(of(new Category(2L, new User().setId(1L), "salary")));
        when(accountRepository.findByIdAndUserId(1L, 1L)).thenReturn(of(new Account(1L, "account_from", 5000, new User().setId(1L))));
        when(accountRepository.findByIdAndUserId(2L, 1L)).thenReturn(of(new Account(2L, "account_to", 7000, new User().setId(1L))));
        when(accountRepository.getReferenceById(1L)).thenReturn(new Account(1L, "account_from", 5000, new User().setId(1L)));
        when(accountRepository.getReferenceById(2L)).thenReturn(new Account(2L, "account_to", 7000, new User().setId(1L)));

        List<Long> categories = new ArrayList<>();
        categories.add(2L);
        List<Category> categoryEntities = categories.stream().map(x -> categoryRepository.getReferenceById(x)).collect(toList());

        Transaction transaction = new Transaction(1L, accountRepository.getReferenceById(1L),
                accountRepository.getReferenceById(2L), 100,
                new Date(2021 - 1900, 12 - 1, 10), categoryEntities);

        when(transactionRepository.save(any(Transaction.class))).thenReturn(transaction);

        TransactionDTO transactionExpected = new TransactionDTO(1, 1L, 2L, 100,
                new Date(2021 - 1900, 12 - 1, 10));

        when(transactionDtoConverter.convert(any(Transaction.class))).thenReturn(transactionExpected);

        TransactionDTO transactionProvided = subj.create(1, 1, 2, 100,
                new Date(2021 - 1900, 12 - 1, 10), categories);

        assertEquals(transactionExpected, transactionProvided);

        verify(transactionRepository, times(1)).save(any(Transaction.class));
        verify(transactionDtoConverter, times(1)).convert(any(Transaction.class));
    }

    @Test (expected = CustomException.class)
    public void create_wrongAccountId() {
        List<Long> categories = new ArrayList<Long>();
        categories.add(2L);
        List<Category> categoryEntities = categories.stream().map(x -> categoryRepository.getReferenceById(x)).collect(toList());
        Transaction transactionSaved = new Transaction(1L, accountRepository.getReferenceById(3L),
                accountRepository.getReferenceById(2L), 100L,
                new Date(2021 - 1900, 12 - 1, 10), categoryEntities);

        TransactionDTO transactionDTO = subj.create(1, 3, 2, 100,
                new Date(2021 - 1900, 12 - 1, 10), categories);

        verify(transactionRepository, times(1)).save(transactionSaved);
        verifyNoInteractions(transactionDtoConverter);
    }

    @Test (expected = CustomException.class)
    public void create_twoNullAccountIds() {
        List<Long> categories = new ArrayList<Long>();
        categories.add(2L);
        List<Category> categoryEntities = categories.stream().map(x -> categoryRepository.getReferenceById(x)).collect(toList());
        Transaction transactionSaved = new Transaction(1L, accountRepository.getReferenceById(0L),
                accountRepository.getReferenceById(0L), 100L,
                new Date(2021 - 1900, 12 - 1, 10), categoryEntities);

        TransactionDTO transactionDTO = subj.create(1, 0, 0, 100,
                new Date(2021 - 1900, 12 - 1, 10), categories);

        verify(transactionRepository, times(1)).save(transactionSaved);
        verifyNoInteractions(transactionDtoConverter);
    }

    @Test (expected = CustomException.class)
    public void create_wrongCategoryId() {
        List<Long> categories = new ArrayList<Long>();
        categories.add(0L);
        List<Category> categoryEntities = categories.stream().map(x -> categoryRepository.getReferenceById(x)).collect(toList());
        Transaction transactionSaved = new Transaction(1L, accountRepository.getReferenceById(3L),
                accountRepository.getReferenceById(2L), 100L,
                new Date(2021 - 1900, 12 - 1, 10), categoryEntities);

        TransactionDTO transactionDTO = subj.create(1, 3, 2, 100,
                new Date(2021 - 1900, 12 - 1, 10), categories);

        verify(transactionRepository, times(1)).save(transactionSaved);
        verifyNoInteractions(transactionDtoConverter);
    }
}
package ru.kuzmin.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.kuzmin.api.converter.CategoryEntityToCategoryDtoConverter;
import ru.kuzmin.api.converter.CategoryReportModelToCategoryReportDto;
import ru.kuzmin.entity.Category;
import ru.kuzmin.entity.User;
import ru.kuzmin.exception.CustomException;
import ru.kuzmin.repository.CategoryReportModel;
import ru.kuzmin.repository.CategoryRepository;
import ru.kuzmin.repository.UserRepository;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.singleton;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static ru.kuzmin.security.UserRole.USER;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceTest {
    @InjectMocks CategoryService subj;
    @Mock CategoryRepository categoryRepository;
    @Mock UserRepository userRepository;
    @Mock CategoryEntityToCategoryDtoConverter categoryDtoConverter;
    @Mock CategoryReportModelToCategoryReportDto reportModelToCategoryReportDto;

    @Before
    public void setUp() {
        subj = new CategoryService(categoryRepository, userRepository, categoryDtoConverter, reportModelToCategoryReportDto);
    }

    @Test
    public void findAll_categoriesFound() {
        List<Category> categoryEntities = new ArrayList<>();
        User user = userRepository.findById(1L).orElse(null);
        Category category = new Category(1L, user, "name");
        categoryEntities.add(category);
        when(categoryRepository.findCategoriesByUserId(1)).thenReturn(categoryEntities);

        List<CategoryDTO> categoryDTOSExpected = new ArrayList<>();
        CategoryDTO categoryExpected = new CategoryDTO(1, 1, "name");
        categoryDTOSExpected.add(categoryExpected);
        when(categoryDtoConverter.convert(category)).thenReturn(categoryExpected);

        List<CategoryDTO> categoryDTOSProvided = subj.findAll(1);
        CategoryDTO categoryProvided = categoryDTOSProvided.get(0);

        assertEquals(categoryDTOSExpected, categoryDTOSProvided);
        assertEquals(categoryExpected, categoryProvided);

        verify(categoryRepository, times(1)).findCategoriesByUserId(1);
        verify(categoryDtoConverter, times(1)).convert(category);
    }

    @Test
    public void findAll_categoriesNotFound() {
        List<Category> categoryEntities = new ArrayList<>();
        when(categoryRepository.findCategoriesByUserId(1)).thenReturn(categoryEntities);

        List<CategoryDTO> categoryDTOSExpected = new ArrayList<>();
        List<CategoryDTO> categoryDTOSProvided = subj.findAll(1);

        assertEquals(categoryDTOSExpected, categoryDTOSProvided);
        assertTrue(categoryDTOSProvided.isEmpty());

        verify(categoryRepository, times(1)).findCategoriesByUserId(1);
        verifyNoInteractions(categoryDtoConverter);
    }

    @Test
    public void create_categoryInserted() {
        when(userRepository.findById(1L)).thenReturn(
                Optional.ofNullable(new User().setId(1L)
                        .setEmail("123@mail.ru")
                        .setPassword("123")
                        .setRoles(singleton(USER)))
        );

        User user = userRepository.findById(1L).orElse(null);
        Category category = new Category(1L, user, "name");
        Category categorySaved = new Category(user, "name");
        when(categoryRepository.save(categorySaved)).thenReturn(category);

        CategoryDTO categoryExpected = new CategoryDTO(1, 1, "name");
        when(categoryDtoConverter.convert(category)).thenReturn(categoryExpected);

        CategoryDTO categoryProvided = subj.create(1L, "name");

        assertEquals(categoryExpected, categoryProvided);

        verify(categoryRepository, times(1)).save(categorySaved);
        verify(categoryDtoConverter, times(1)).convert(category);
    }

    @Test (expected = CustomException.class)
    public void create_categoryNotInserted() {
        User user = userRepository.findById(1L).orElse(null);
        Category categorySaved = new Category(user, "name");

        CategoryDTO category = subj.create(1, "name");

        verify(categoryRepository, times(1)).save(categorySaved);
        verifyNoInteractions(categoryDtoConverter);
    }

    @Test
    public void incomeReport_found() {
        List<CategoryReportModel> reportModels = new ArrayList<>();
        CategoryReportModel reportModel = new CategoryReportModel("name", 1000);
        reportModels.add(reportModel);
        when(categoryRepository.incomeReport(1, new Date(2000 - 1900, 10, 30),
                new Date(2022 - 1900, 10, 30))).thenReturn(reportModels);

        List<CategoryReportDTO> reportDTOSExpected = new ArrayList<>();
        CategoryReportDTO reportExpected = new CategoryReportDTO("name", 1000);
        reportDTOSExpected.add(reportExpected);
        when(reportModelToCategoryReportDto.convert(reportModel)).thenReturn(reportExpected);

        List<CategoryReportDTO> reportDTOSProvided = subj.incomeReport(1,
                new Date(2000 - 1900, 10, 30), new Date(2022 - 1900, 10, 30));
        CategoryReportDTO reportProvided = reportDTOSProvided.get(0);

        assertEquals(reportDTOSExpected, reportDTOSProvided);
        assertEquals(reportExpected, reportProvided);

        verify(categoryRepository, times(1)).incomeReport(1,
                new Date(2000 - 1900, 10, 30), new Date(2022 - 1900, 10, 30));
        verify(reportModelToCategoryReportDto, times(1)).convert(reportModel);
    }

    @Test
    public void incomeReport_NotFound() {
        List<CategoryReportModel> reportModels = new ArrayList<>();
        when(categoryRepository.incomeReport(1, new Date(2000 - 1900, 10, 30),
                new Date(2022 - 1900, 10, 30))).thenReturn(reportModels);

        List<CategoryReportDTO> reportDTOSExpected = new ArrayList<>();
        List<CategoryReportDTO> reportDTOSProvided = subj.incomeReport(1,
                new Date(2000 - 1900, 10, 30), new Date(2022 - 1900, 10, 30));

        assertEquals(reportDTOSExpected, reportDTOSProvided);
        assertTrue(reportDTOSProvided.isEmpty());

        verify(categoryRepository, times(1)).incomeReport(1,
                new Date(2000 - 1900, 10, 30), new Date(2022 - 1900, 10, 30));
        verifyNoInteractions(reportModelToCategoryReportDto);
    }

    @Test
    public void outcomeReport_found() {
        List<CategoryReportModel> reportModels = new ArrayList<>();
        CategoryReportModel reportModel = new CategoryReportModel("name", 1000);
        reportModels.add(reportModel);
        when(categoryRepository.outcomeReport(1, new Date(2000 - 1900, 10, 30),
                new Date(2022 - 1900, 10, 30))).thenReturn(reportModels);

        List<CategoryReportDTO> reportDTOSExpected = new ArrayList<>();
        CategoryReportDTO reportExpected = new CategoryReportDTO("name", 1000);
        reportDTOSExpected.add(reportExpected);
        when(reportModelToCategoryReportDto.convert(reportModel)).thenReturn(reportExpected);

        List<CategoryReportDTO> reportDTOSProvided = subj.outcomeReport(1,
                new Date(2000 - 1900, 10, 30), new Date(2022 - 1900, 10, 30));
        CategoryReportDTO reportProvided = reportDTOSProvided.get(0);

        assertEquals(reportDTOSExpected, reportDTOSProvided);
        assertEquals(reportExpected, reportProvided);

        verify(categoryRepository, times(1)).outcomeReport(1,
                new Date(2000 - 1900, 10, 30), new Date(2022 - 1900, 10, 30));
        verify(reportModelToCategoryReportDto, times(1)).convert(reportModel);
    }

    @Test
    public void outcomeReport_NotFound() {
        List<CategoryReportModel> reportModels = new ArrayList<>();
        when(categoryRepository.outcomeReport(1, new Date(2000 - 1900, 10, 30),
                new Date(2022 - 1900, 10, 30))).thenReturn(reportModels);

        List<CategoryReportDTO> reportDTOSExpected = new ArrayList<>();
        List<CategoryReportDTO> reportDTOSProvided = subj.outcomeReport(1,
                new Date(2000 - 1900, 10, 30), new Date(2022 - 1900, 10, 30));

        assertEquals(reportDTOSExpected, reportDTOSProvided);
        assertTrue(reportDTOSProvided.isEmpty());

        verify(categoryRepository, times(1)).outcomeReport(1,
                new Date(2000 - 1900, 10, 30), new Date(2022 - 1900, 10, 30));
        verifyNoInteractions(reportModelToCategoryReportDto);
    }
}
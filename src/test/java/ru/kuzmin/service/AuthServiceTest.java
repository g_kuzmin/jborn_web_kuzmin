package ru.kuzmin.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.kuzmin.api.converter.UserEntityToUserDtoConverter;
import ru.kuzmin.entity.User;
import ru.kuzmin.exception.CustomException;
import ru.kuzmin.repository.UserRepository;

import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthServiceTest {
    @InjectMocks AuthService subj;
    @Mock UserRepository userRepository;
    @Mock PasswordEncoder passwordEncoder;
    @Mock UserEntityToUserDtoConverter userDtoConverter;

    @Test
    public void auth_userFound() {
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);

        User userEntity = new User(1, "123@mail.ru", "$2a$12$z2J8jpANMyksgjm77qvqLOo9v7CNC0pqNTsW.UUidskAaUAVDTaSu");
        when(userRepository.findByEmail(anyString())).thenReturn(of(userEntity));

        UserDTO userDTO = new UserDTO(1, "123@mail.ru");
        when(userDtoConverter.convert(userEntity)).thenReturn(userDTO);

        UserDTO user = subj.auth("123@mail.ru", "123");

        assertNotNull(user);
        assertEquals(userDTO, user);

        verify(userRepository, times(1)).findByEmail("123@mail.ru");
        verify(userDtoConverter, times(1)).convert(userEntity);
    }

    @Test (expected = CustomException.class)
    public void auth_userNotFound() {
        when(userRepository.findByEmail("123@mail.ru")).thenThrow(CustomException.class);

        UserDTO user = subj.auth("123@mail.ru", "password");

        verify(passwordEncoder, times(1)).encode("password");
        verify(userRepository, times(1)).findByEmail("123@mail.ru");
        verifyNoInteractions(userDtoConverter);
    }

    @Test
    public void registration_userInserted() {
        when(passwordEncoder.encode("password")).thenReturn("hex");

        User userEntity = new User(1, "123@mail.ru", "hex");
        when(userRepository.save(any(User.class))).thenReturn(userEntity);

        UserDTO userDTO = new UserDTO(1, "123@mail.ru");
        when(userDtoConverter.convert(userEntity)).thenReturn(userDTO);

        UserDTO user = subj.registration("123@mail.ru", "password");

        assertEquals(userDTO, user);

        verify(passwordEncoder, times(1)).encode("password");
        verify(userRepository, times(1)).save(any(User.class));
        verify(userDtoConverter, times(1)).convert(userEntity);
    }

    @Test (expected = CustomException.class)
    public void registration_userNotInserted() {
        when(passwordEncoder.encode("123")).thenReturn("123");

        User userEntitySaved = new User("123@mail.ru", "123");
        when(userRepository.save(any(User.class))).thenThrow(CustomException.class);

        UserDTO user = subj.registration("123@mail.ru", "123");

        verify(passwordEncoder, times(1)).encode("123");
        verify(userRepository, times(1)).save(userEntitySaved);
        verifyNoInteractions(userDtoConverter);
    }
}
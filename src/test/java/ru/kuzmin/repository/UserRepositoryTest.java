package ru.kuzmin.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kuzmin.entity.User;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@RunWith(SpringRunner.class)
public class UserRepositoryTest {
    @Autowired UserRepository subj;

    @Test
    public void findByEmail() {
        User user = subj.findByEmail("123@mail.ru").orElse(null);

        assertNotNull(user);
        assertEquals(1L, user.getId());
        assertEquals("123@mail.ru", user.getEmail());
        assertEquals("$2a$12$z2J8jpANMyksgjm77qvqLOo9v7CNC0pqNTsW.UUidskAaUAVDTaSu", user.getPassword());
    }
}
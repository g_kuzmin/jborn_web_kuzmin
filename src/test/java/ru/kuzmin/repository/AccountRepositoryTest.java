package ru.kuzmin.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kuzmin.entity.Account;

import java.util.List;

import static java.lang.Long.valueOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@DataJpaTest
@RunWith(SpringRunner.class)
public class AccountRepositoryTest {
    @Autowired AccountRepository subj;

    @Test
    public void findByIdAndUserId() {
        Account account = subj.findByIdAndUserId(1L, 1L).orElse(null);

        assertNotNull(account);
        assertEquals(valueOf(1L), account.getId());
        assertEquals("bank_account", account.getName());
        assertEquals(50000, account.getBalance());
        assertEquals(valueOf(1L), account.getUser().getId());
    }

    @Test
    public void findAccountsByUserId() {
        List<Account> accounts = subj.findAccountsByUserId(1L);

        assertNotNull(accounts);
        assertEquals(2, accounts.size());
        assertEquals(valueOf(1L), accounts.get(0).getId());
        assertEquals("bank_account", accounts.get(0).getName());
        assertEquals(50000, accounts.get(0).getBalance());
        assertEquals(valueOf(1L), accounts.get(0).getUser().getId());
    }
}
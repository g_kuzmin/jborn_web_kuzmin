package ru.kuzmin.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kuzmin.entity.Category;

import java.sql.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@RunWith(SpringRunner.class)
public class CategoryRepositoryTest {
    @Autowired CategoryRepository subj;

    @Test
    public void findByIdAndUserId() {
        Category category = subj.findByIdAndUserId(1L, 1L).orElse(null);

        assertNotNull(category);
        assertEquals(1L, category.getId());
        assertEquals(1L, category.getUser().getId());
        assertEquals("salary", category.getName());
    }

    @Test
    public void findCategoriesByUserId() {
        List<Category> categories = subj.findCategoriesByUserId(1L);

        assertNotNull(categories);
        assertEquals(2, categories.size());
        assertEquals(1L, categories.get(0).getId());
        assertEquals(1L, categories.get(0).getUser().getId());
        assertEquals("salary", categories.get(0).getName());
    }

    @Test
    public void incomeReport() {
        List<CategoryReportModel> reports = subj.incomeReport(1L, new Date(2000 - 1900, 10, 30),
                new Date(2022 - 1900, 10, 30));

        assertNotNull(reports);
        assertEquals(1, reports.size());
        assertEquals("salary", reports.get(0).getName());
        assertEquals(27800, reports.get(0).getSum());
    }

    @Test
    public void outcomeReport() {
        List<CategoryReportModel> reports = subj.outcomeReport(1L, new Date(2000 - 1900, 10, 30),
                new Date(2022 - 1900, 10, 30));

        assertNotNull(reports);
        assertEquals(1, reports.size());
        assertEquals("salary", reports.get(0).getName());
        assertEquals(6300, reports.get(0).getSum());
    }
}
package ru.kuzmin.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kuzmin.MockSecurityConfiguration;
import ru.kuzmin.config.SecurityConfiguration;
import ru.kuzmin.service.TransactionDTO;
import ru.kuzmin.service.TransactionService;
import ru.kuzmin.web.form.TransactionForm;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(TransactionsController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class TransactionsControllerTest {
    @Autowired MockMvc mockMvc;
    @MockBean TransactionService transactionService;

    @Test
    public void getAddTransaction() throws Exception {
        mockMvc.perform(get("/add-transaction"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("transactionForm", new TransactionForm()))
                .andExpect(view().name("addTransaction"));
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void postAddTransaction() throws Exception {
        when(transactionService.create(anyLong(), anyLong(), anyLong(), anyLong(), any(Date.class), any()))
                .thenReturn(new TransactionDTO(1L, 1L, 2L, 1, new Date(2023 - 1900, 2 - 1, 3)));

        List<Long> categories = new ArrayList<>();
        categories.add(1L);

        mockMvc.perform(post("/add-transaction")
                        .param("fromAccountId", "1")
                        .param("toAccountId", "2")
                        .param("amount", "1")
                        .param("categories", "1"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("transaction", transactionService.create(1L, 1L, 2L, 1, new Date(2023 - 1900, 2 - 1, 3), categories)))
                .andExpect(view().name("addingTransaction"));
    }
}
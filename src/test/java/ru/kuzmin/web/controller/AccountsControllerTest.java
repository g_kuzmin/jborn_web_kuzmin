package ru.kuzmin.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kuzmin.MockSecurityConfiguration;
import ru.kuzmin.config.SecurityConfiguration;
import ru.kuzmin.service.AccountDTO;
import ru.kuzmin.service.AccountService;
import ru.kuzmin.web.form.AccountForm;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AccountsController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class AccountsControllerTest {
    @Autowired MockMvc mockMvc;
    @MockBean AccountService accountService;

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void getAccounts() throws Exception {
        mockMvc.perform(get("/display-accounts"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("accounts", accountService.findAll(1L)))
                .andExpect(view().name("displayAccounts"));
    }

    @Test
    public void getAddAccount() throws Exception {
        mockMvc.perform(get("/add-account"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("accountForm", new AccountForm()))
                .andExpect(view().name("addAccount"));
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void postAddAccount() throws Exception {
        when(accountService.create(anyLong(), anyString(), anyLong()))
                .thenReturn(new AccountDTO(1L, "bank_account", 50000, 1L));

        mockMvc.perform(post("/add-account")
                        .param("name", "bank_account")
                        .param("balance", "50000"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("account", accountService.create(1L, "bank_account", 50000)))
                .andExpect(view().name("addingAccount"));
    }

    @Test
    public void getDeleteAccount() throws Exception {
        mockMvc.perform(get("/delete-account"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("accountForm", new AccountForm()))
                .andExpect(view().name("deleteAccount"));
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void postDeleteAccount() throws Exception {
        mockMvc.perform(post("/delete-account")
                        .param("id", "1"))
                .andExpect(status().isOk())
                .andExpect(view().name("success"));
    }
}
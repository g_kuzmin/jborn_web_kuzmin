package ru.kuzmin.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kuzmin.MockSecurityConfiguration;
import ru.kuzmin.config.SecurityConfiguration;
import ru.kuzmin.entity.Category;
import ru.kuzmin.service.CategoryDTO;
import ru.kuzmin.service.CategoryService;
import ru.kuzmin.web.form.CategoryForm;
import ru.kuzmin.web.form.ReportForm;

import java.util.ArrayList;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CategoriesController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class CategoriesControllerTest {
    @Autowired MockMvc mockMvc;
    @MockBean CategoryService categoryService;
    @SpyBean Converter<Category, CategoryDTO> categoryDtoConverter;

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void getCategories() throws Exception {
        mockMvc.perform(get("/display-categories"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("categories", categoryService.findAll(1L)))
                .andExpect(view().name("displayCategories"));
    }

    @Test
    public void getAddCategory() throws Exception {
        mockMvc.perform(get("/add-category"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("categoryForm", new CategoryForm()))
                .andExpect(view().name("addCategory"));
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void postAddCategory() throws Exception {
        when(categoryService.create(1L, "salary"))
                .thenReturn(new CategoryDTO(1L, 1L, "salary"));

        mockMvc.perform(post("/add-category")
                        .param("name", "salary"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("category", categoryService.create(1L, "salary")))
                .andExpect(view().name("addingCategory"));
    }

    @Test
    public void getUpdateCategory() throws Exception {
        mockMvc.perform(get("/update-category"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("categoryForm", new CategoryForm()))
                .andExpect(view().name("updateCategory"));
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void postUpdateCategory() throws Exception {
        when(categoryService.update(1L, 1L, "updated"))
                .thenReturn(new CategoryDTO(1L, 1L, "updated"));

        mockMvc.perform(post("/update-category")
                        .param("id", "1")
                        .param("name", "updated"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("updatedCategory", categoryService.update(1L, 1L, "updated")))
                .andExpect(view().name("updatingCategory"));
    }

    @Test
    public void getDeleteCategory() throws Exception {
        mockMvc.perform(get("/delete-category"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("categoryForm", new CategoryForm()))
                .andExpect(view().name("deleteCategory"));
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void postDeleteCategory() throws Exception {
        mockMvc.perform(post("/delete-category"))
                .andExpect(status().isOk())
                .andExpect(view().name("success"));
    }

    @Test
    public void getIncomeReport() throws Exception {
        mockMvc.perform(get("/income"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("reportForm", new ReportForm()))
                .andExpect(view().name("incomeReport"));
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void postIncomeReport() throws Exception {
        mockMvc.perform(post("/income"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("incomeReports", new ArrayList<>()))
                .andExpect(view().name("displayIncomeReport"));
    }

    @Test
    public void getOutcomeReport() throws Exception {
        mockMvc.perform(get("/outcome"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("reportForm", new ReportForm()))
                .andExpect(view().name("outcomeReport"));
    }

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void postOutcomeReport() throws Exception {
        mockMvc.perform(post("/outcome"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("outcomeReports", new ArrayList<>()))
                .andExpect(view().name("displayOutcomeReport"));
    }
}
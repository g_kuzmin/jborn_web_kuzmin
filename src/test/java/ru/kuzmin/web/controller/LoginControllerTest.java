package ru.kuzmin.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kuzmin.MockSecurityConfiguration;
import ru.kuzmin.config.SecurityConfiguration;
import ru.kuzmin.entity.User;
import ru.kuzmin.service.AuthService;
import ru.kuzmin.web.form.LoginForm;

import static java.util.Collections.singleton;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.kuzmin.security.UserRole.USER;

@WebMvcTest(LoginController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
public class LoginControllerTest {
    @Autowired MockMvc mockMvc;
    @MockBean AuthService authService;

    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void index() throws Exception {
        when(authService.getCurrentUser(1L)).thenReturn(
                new User().setId(1L)
                        .setEmail("123@mail.ru")
                        .setPassword("123")
                        .setRoles(singleton(USER))
        );

        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("id", 1L))
                .andExpect(model().attribute("name", "123@mail.ru"))
                .andExpect(view().name("personal-area"));
    }

    @Test
    public void getLogin() throws Exception {
        mockMvc.perform(get("/login-form"))
                .andExpect(status().isOk())
                .andExpect(view().name("login-form"));
    }

    @Test
    public void getRegister() throws Exception {
        mockMvc.perform(get("/register"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("loginForm", new LoginForm()))
                .andExpect(view().name("register"));
    }


    @WithUserDetails(value = "123@mail.ru", userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void postRegister() throws Exception {
        mockMvc.perform(post("/register")
                        .param("email", "123@mail.ru")
                        .param("password", "123"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/"));
    }
}
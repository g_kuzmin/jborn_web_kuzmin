package ru.kuzmin.security;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kuzmin.Runner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static ru.kuzmin.security.UserRole.USER;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CustomUserDetailsServiceTest {
    @Autowired CustomUserDetailsService subj;
    @MockBean Runner runner;

    @Test
    public void loadUserByUsername() {
        UserDetails userDetails = subj.loadUserByUsername("123@mail.ru");

        assertNotNull(userDetails);
        assertEquals("123@mail.ru", userDetails.getUsername());
        assertEquals("$2a$12$z2J8jpANMyksgjm77qvqLOo9v7CNC0pqNTsW.UUidskAaUAVDTaSu", userDetails.getPassword());
        assertEquals(1, userDetails.getAuthorities().size());
        assertEquals(new CustomGrantedAuthority(USER), userDetails.getAuthorities().iterator().next());
    }
}
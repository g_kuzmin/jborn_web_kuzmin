package ru.kuzmin;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.kuzmin.exception.CustomException;
import ru.kuzmin.service.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Component
@RequiredArgsConstructor
public class Runner implements CommandLineRunner{
    private final AuthService authService;
    private final AccountService accountService;
    private final TransactionService transactionService;
    private final CategoryService categoryService;

    private final
    static String greet = "Добрый день! Что вы хотите сделать?\n1 - Регистрация\n2 - Вход в систему\n0 - Выход";
    static String showOperations = "Выберите тип операций:\n1 - Операции со счетами\n2 - Операции с типами транзакций\n0 - Выход";
    static String showMovesWithAccounts = "1 - Отобразить все счета\n2 - Создать новый счет" +
            "\n3 - Удалить счет\n4 - Добавить транзакцию\n5 - Вернуться к типам операций\n0 - Выход";
    static String showMovesWithCategories = "1 - Отобразить все типы транзакций\n2 - Создать тип транзакции" +
            "\n3 - Обновить тип транзакции\n4 - Удалить тип транзакции\n5 - Вывести отчет по категориям\n6 - Вернуться к типам операций\n0 - Выход";

    @Override
    public void run(String... args) throws Exception {
        showMainScreen();
    }

    void showMainScreen() {
        UserDTO userDTO;

        int id = 0;
        String s;
        do {
            s = request(greet);
        } while (!s.equals("1") && !s.equals("2") && !s.equals("0"));

        String email;
        String password;

        switch (s) {
            case "1":
                email = request("Введите email:");
                password = request("Введите пароль:");

                try {
                    userDTO = authService.registration(email, password);
                    System.out.println("Добрый день, " + userDTO.getEmail() + "! Вы успешно зарегистрированы.");
                    chooseOperation(userDTO.getId());
                } catch (CustomException e) {
                    System.out.println(e.getMessage());
                    showMainScreen();
                }
                break;
            case "2":
                email = request("Введите email:");
                password = request("Введите пароль:");

                try {
                    userDTO = authService.auth(email, password);
                    System.out.println("Добрый день, " + userDTO.getEmail() + "! Вы успешно вошли в систему.");
                    chooseOperation(userDTO.getId());
                } catch (CustomException e) {
                    System.out.println(e.getMessage());
                    showMainScreen();
                }
                break;
            case "0":
                break;
        }
    }

    void chooseOperation(long userId) {
        String s;
        do {
            s = request(showOperations);
        } while (!s.equals("1") && !s.equals("2") && !s.equals("0"));

        switch (s) {
            case "1":
                operateWithAccounts(userId);
                break;
            case "2":
                operateWithCategories(userId);
                break;
            case "0":
                break;
        }
    }

    void operateWithAccounts(long userId) {
        AccountDTO accountDTO;
        TransactionDTO transactionDTO;

        String s;
        do {
            s = request(showMovesWithAccounts);
        } while (!s.equals("1") && !s.equals("2") && !s.equals("3")
                && !s.equals("4") && !s.equals("5") && !s.equals("0"));

        switch (s) {
            case "1":
                List<AccountDTO> accounts = accountService.findAll(userId);
                if (accounts.isEmpty()) {
                    System.out.println("Нет доступных счетов");
                } else {
                    accounts.forEach(System.out::println);
                }
                operateWithAccounts(userId);
                break;
            case "2":
                String name = request("Введите название счета:");
                String balanceString;
                do {
                    balanceString = request("Введите баланс счета:");
                    if (!balanceString.matches("\\d+")) {
                        System.out.println("Баланс счета должен быть числом!");
                    }
                } while (!balanceString.matches("\\d+"));
                long balance = Long.parseLong(balanceString);

                try {
                    accountDTO = accountService.create(userId, name, balance);
                    System.out.println("Счет успешно создан");
                } catch (CustomException e) {
                    System.out.println(e.getMessage());
                }
                operateWithAccounts(userId);
                break;
            case "3":
                String accountIdString;
                do {
                    accountIdString = request("Введите id счета для удаления:");
                    if (!accountIdString.matches("\\d+")) {
                        System.out.println("id счета должен состоять из цифр!");
                    }
                } while (!accountIdString.matches("\\d+"));
                int accountId = Integer.parseInt(accountIdString);

                try {
                    accountService.remove(userId, accountId);
                    System.out.println("Счет успешно удален");
                } catch (CustomException e) {
                    System.out.println(e.getMessage());
                }
                operateWithAccounts(userId);
                break;
            case "4":
                String fromAccountIdString;
                String toAccountIdString;
                String amountString;
                Date createdDate = new Date(System.currentTimeMillis());
                String categoryIdString;
                List<Long> categories = new ArrayList<>();
                do {
                    fromAccountIdString = request("Введите id аккаунта, с которого переводятся деньги, " +
                            "или 0, если перевод идет с внешнего аккаунта:");
                    if (!fromAccountIdString.matches("\\d+")) {
                        System.out.println("id должен состоять из цифр!");
                    }
                } while (!fromAccountIdString.matches("\\d+"));
                long fromAccountId = Long.parseLong(fromAccountIdString);

                do {
                    toAccountIdString = request("Введите id аккаунта, на который переводятся деньги, " +
                            "или 0, если перевод идет на внешний аккаунт:");
                    if (!toAccountIdString.matches("\\d+")) {
                        System.out.println("id должен состоять из цифр!");
                    }
                } while (!toAccountIdString.matches("\\d+"));
                long toAccountId = Long.parseLong(toAccountIdString);

                do {
                    amountString = request("Введите сумму перевода:");
                    if (!amountString.matches("\\d+")) {
                        System.out.println("Сумма должна состоять из цифр!");
                    }
                } while (!amountString.matches("\\d+"));
                long amount = Long.parseLong(amountString);

                while (true) {
                    categoryIdString = request("Введите id типа транзакции для перевода или 'q' для окончания ввода:");
                    if (categoryIdString.equals("q")) {
                        break;
                    }

                    if (!categoryIdString.matches("\\d+")) {
                        System.out.println("id должен состоять из цифр!");
                    } else {
                        categories.add(Long.parseLong(categoryIdString));
                    }
                }

                try {
                    transactionDTO = transactionService.create(userId, fromAccountId, toAccountId,
                            amount, createdDate, categories);
                    System.out.println("Транзакция успешно создана");
                } catch (CustomException e) {
                    System.out.println(e.getMessage());
                }

                operateWithAccounts(userId);
                break;
            case "5":
                chooseOperation(userId);
                break;
            case "0":
                break;
        }
    }

    void operateWithCategories(long userId) {
        CategoryDTO categoryDTO;
        long categoryId;
        String categoryIdString;

        String s;
        do {
            s = request(showMovesWithCategories);
        } while (!s.equals("1") && !s.equals("2") && !s.equals("3")
                && !s.equals("4") && !s.equals("5") && !s.equals("6") && !s.equals("0"));

        switch (s) {
            case "1":
                List<CategoryDTO> categories = categoryService.findAll(userId);
                if (categories.isEmpty()) {
                    System.out.println("Нет доступных типов транзакций");
                } else {
                    categories.forEach(System.out::println);
                }
                operateWithCategories(userId);
                break;
            case "2":
                String name = request("Введите название типа транзакий:");
                try {
                    categoryDTO = categoryService.create(userId, name);
                    System.out.println("Тип транзакций успешно создан");
                } catch (CustomException e) {
                    System.out.println(e.getMessage());
                }
                operateWithCategories(userId);
                break;
            case "3":
                do {
                    categoryIdString = request("Введите id типа транзакции для обновления:");
                    if (!categoryIdString.matches("\\d+")) {
                        System.out.println("id типа транзакции должен состоять из цифр!");
                    }
                } while (!categoryIdString.matches("\\d+"));
                categoryId = Long.parseLong(categoryIdString);
                String nameToUpdate = request("Введите новое название типа транзакции:");

                try {
                    categoryDTO = categoryService.update(categoryId, userId, nameToUpdate);
                    System.out.println("Тип транзакций успешно обновлен");
                } catch (CustomException e) {
                    System.out.println(e.getMessage());
                }
                operateWithCategories(userId);
                break;
            case "4":
                do {
                    categoryIdString = request("Введите id типа транзакции для удаления:");
                    if (!categoryIdString.matches("\\d+")) {
                        System.out.println("id типа транзакции должен состоять из цифр!");
                    }
                } while (!categoryIdString.matches("\\d+"));
                categoryId = Long.parseLong(categoryIdString);

                try {
                    categoryService.remove(categoryId, userId);
                    System.out.println("Тип транзакции удален");
                } catch (CustomException e) {
                    System.out.println(e.getMessage());
                }
                operateWithCategories(userId);
                break;
            case "5":
                String beginDay;
                String beginMonth;
                String beginYear;

                do {
                    beginDay = request("Введите день начала:");
                    beginMonth = request("Введите месяц начала:");
                    beginYear = request("Введите год начала:");
                    if (!beginDay.matches("\\d+") && !beginMonth.matches("\\d+") && !beginYear.matches("\\d+")) {
                        System.out.println("День, месяц и год должны состоять из цифр!");
                    }
                } while (!beginDay.matches("\\d+") && !beginMonth.matches("\\d+") && !beginYear.matches("\\d+"));
                Date beginDate = new Date(Integer.parseInt(beginYear) - 1900, Integer.parseInt(beginMonth) - 1, Integer.parseInt(beginDay));

                String endDay;
                String endMonth;
                String endYear;

                do {
                    endDay = request("Введите день окончания:");
                    endMonth = request("Введите месяц окончания:");
                    endYear = request("Введите год окончания:");
                    if (!endDay.matches("\\d+") && !endMonth.matches("\\d+") && !endYear.matches("\\d+")) {
                        System.out.println("День, месяц и год должны состоять из цифр!");
                    }
                } while (!endDay.matches("\\d+") && !endMonth.matches("\\d+") && !endYear.matches("\\d+"));
                Date endDate = new Date(Integer.parseInt(endYear) - 1900, Integer.parseInt(endMonth) - 1, Integer.parseInt(endDay));

                String choseTypeOfReport;
                do {
                    choseTypeOfReport = request("Какой отчет вы хотите отобразить?\n1 - Доход\n2 - Расход");
                } while (!choseTypeOfReport.equals("1") && !choseTypeOfReport.equals("2"));

                switch (choseTypeOfReport) {
                    case "1":
                        List<CategoryReportDTO> incomeReports = categoryService.incomeReport(userId, beginDate, endDate);
                        if (incomeReports.isEmpty()) {
                            System.out.println("По типам транзакций нет дохода");
                        } else {
                            incomeReports.forEach(System.out::println);
                        }
                        operateWithCategories(userId);
                        break;
                    case "2":
                        List<CategoryReportDTO> outcomeReports = categoryService.outcomeReport(userId, beginDate, endDate);
                        if (outcomeReports.isEmpty()) {
                            System.out.println("По типам транзакций нет расхода");
                        } else {
                            outcomeReports.forEach(System.out::println);
                        }
                        operateWithCategories(userId);
                        break;
                }
                break;
            case "6":
                chooseOperation(userId);
                break;
            case "0":
                break;
        }
    }

    String request(String title) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(title);
        return scanner.next();
    }
}

package ru.kuzmin.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Data
@Accessors(chain = true)
@Entity
@Table(name = "transaction")
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "from_account_id")
    private Account fromAccount;

    @ManyToOne
    @JoinColumn(name = "to_account_id")
    private Account toAccount;

    @Column(name = "amount")
    private long amount;

    @Column(name = "created_date")
    private Date createdDate;

    @ManyToMany
    @JoinTable(
            name = "category_to_transaction",
            joinColumns = @JoinColumn(name = "transaction_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id")
    )
    private List<Category> categories;

    public Transaction(Account fromAccount, Account toAccount, long amount, Date createdDate, List<Category> categories) {
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
        this.createdDate = createdDate;
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", fromAccountId=" + (fromAccount == null ? null : fromAccount.getId()) +
                ", toAccountId=" + (toAccount == null ? null : toAccount.getId()) +
                ", amount=" + amount +
                ", createdDate=" + createdDate +
                '}';
    }
}

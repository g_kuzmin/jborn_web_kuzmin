package ru.kuzmin.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import ru.kuzmin.repository.CategoryReportModel;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@Accessors(chain = true)
@Entity
@Table(name = "category")
@NoArgsConstructor
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "consumer_id")
    private User user;

    @Column(name = "name")
    private String name;

    @ManyToMany
    @JoinTable(
            name = "category_to_transaction",
            joinColumns = @JoinColumn(name = "transaction_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id")
    )
    private List<Transaction> transactions;

    public Category(Long id, User user, String name) {
        this.id = id;
        this.user = user;
        this.name = name;
    }

    public Category(User user, String name) {
        this.user = user;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", userId=" + user.getId() +
                ", name='" + name + '\'' +
                '}';
    }
}

package ru.kuzmin.repository;

import lombok.RequiredArgsConstructor;
import ru.kuzmin.exception.CustomException;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class CategoryRepositoryImpl implements CategoryRepositoryCustom {
    private final DataSource dataSource;

    @Override
    public List<CategoryReportModel> incomeReport(long userId, Date beginDate, Date endDate) {
        List<CategoryReportModel> reports = new ArrayList<>();
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps =
                     conn.prepareStatement(
                             "select c.name, sum(amount) as sum " +
                                     "from transaction t" +
                                     "         join category_to_transaction ctt on t.id = ctt.transaction_id" +
                                     "         join category c on c.id = ctt.category_id " +
                                     "where consumer_id = ?" +
                                     "  and t.from_account_id is null " +
                                     "  and (t.created_date between ?::timestamp and ?::timestamp) " +
                                     "group by c.name;")) {
            ps.setLong(1, userId);
            ps.setDate(2, beginDate);
            ps.setDate(3, endDate);

            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                reports.add(new CategoryReportModel(rs.getString("name"), rs.getLong("sum")));
            }
        } catch (SQLException e) {
            throw new CustomException(e);
        }
        return reports;
    }

    @Override
    public List<CategoryReportModel> outcomeReport(long userId, Date beginDate, Date endDate) {
        List<CategoryReportModel> reports = new ArrayList<>();
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps =
                     conn.prepareStatement(
                             "select c.name, sum(amount) as sum " +
                                     "from transaction t" +
                                     "         join category_to_transaction ctt on t.id = ctt.transaction_id" +
                                     "         join category c on c.id = ctt.category_id " +
                                     "where consumer_id = ?" +
                                     "  and t.to_account_id is null " +
                                     "  and (t.created_date between ?::timestamp and ?::timestamp) " +
                                     "group by c.name;")) {
            ps.setLong(1, userId);
            ps.setDate(2, beginDate);
            ps.setDate(3, endDate);

            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                reports.add(new CategoryReportModel(rs.getString("name"), rs.getLong("sum")));
            }
        } catch (SQLException e) {
            throw new CustomException(e);
        }
        return reports;
    }
}

package ru.kuzmin.repository;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

@Data
@AllArgsConstructor
public class CategoryReportModel {
    private String name;
    private long sum;
}

package ru.kuzmin.repository;

import javax.sql.DataSource;
import java.sql.Date;
import java.util.List;

public interface CategoryRepositoryCustom {
    List<CategoryReportModel> incomeReport(long userId, Date beginDate, Date endDate);

    List<CategoryReportModel> outcomeReport(long userId, Date beginDate, Date endDate);
}

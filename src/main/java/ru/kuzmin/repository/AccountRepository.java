package ru.kuzmin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kuzmin.entity.Account;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByIdAndUserId(long id, long userId);

    List<Account> findAccountsByUserId(long userId);
}

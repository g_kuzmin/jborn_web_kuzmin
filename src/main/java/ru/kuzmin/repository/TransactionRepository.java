package ru.kuzmin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kuzmin.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
}

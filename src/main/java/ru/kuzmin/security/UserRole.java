package ru.kuzmin.security;

public enum UserRole {
    USER,
    ADMIN
}

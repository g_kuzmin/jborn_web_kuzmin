package ru.kuzmin.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.kuzmin.api.controller.BaseController;
import ru.kuzmin.service.TransactionDTO;
import ru.kuzmin.service.TransactionService;
import ru.kuzmin.web.form.TransactionForm;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.sql.Date;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor
@Controller
public class TransactionsController extends BaseController {
    private final TransactionService transactionService;

    @GetMapping("/add-transaction")
    public String getAddTransaction(Model model) {
        model.addAttribute("transactionForm", new TransactionForm());
        return "addTransaction";
    }

    @PostMapping("/add-transaction")
    public String postAddTransaction(@ModelAttribute("transactionForm") @Valid TransactionForm form,
                                  BindingResult result,
                                  Model model,
                                  HttpServletRequest request) {
        if (!result.hasErrors()) {
            TransactionDTO transaction = transactionService.create(
                    getCurrentUserId(),
                    form.getFromAccountId(),
                    form.getToAccountId(),
                    form.getAmount(),
                    new Date(System.currentTimeMillis()),
                    stream(form.getCategories()
                            .split(" "))
                            .map(Long::parseLong)
                            .collect(toList())
            );

            transaction.setFromAccountId(form.getFromAccountId());
            transaction.setToAccountId(form.getToAccountId());

            model.addAttribute("transaction", transaction);
            return "addingTransaction";
        }

        model.addAttribute("transactionForm", form);
        return "addTransaction";
    }
}

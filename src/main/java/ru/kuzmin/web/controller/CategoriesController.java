package ru.kuzmin.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.kuzmin.api.controller.BaseController;
import ru.kuzmin.service.CategoryDTO;
import ru.kuzmin.service.CategoryReportDTO;
import ru.kuzmin.service.CategoryService;
import ru.kuzmin.web.form.CategoryForm;
import ru.kuzmin.web.form.ReportForm;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@Controller
public class CategoriesController extends BaseController {
    private final CategoryService categoryService;

    @GetMapping("/display-categories")
    public String getCategories(Model model, HttpServletRequest request) {
        List<CategoryDTO> categories = categoryService.findAll(getCurrentUserId());

        model.addAttribute("categories", categories);
        return "displayCategories";
    }

    @GetMapping("/add-category")
    public String getAddCategory(Model model) {
        model.addAttribute("categoryForm", new CategoryForm());
        return "addCategory";
    }

    @PostMapping("/add-category")
    public String postAddCategory(@ModelAttribute("categoryForm") @Valid CategoryForm form,
                                  BindingResult result,
                                  Model model) {
        if (!result.hasErrors()) {
            CategoryDTO category = categoryService.create(getCurrentUserId(), form.getName());

            model.addAttribute("category", category);
            return "addingCategory";
        }

        model.addAttribute("categoryForm", form);
        return "addCategory";
    }

    @GetMapping("/update-category")
    public String getUpdateCategory(Model model) {
        model.addAttribute("categoryForm", new CategoryForm());
        return "updateCategory";
    }

    @PostMapping("/update-category")
    public String postUpdateCategory(@ModelAttribute("categoryForm") @Valid CategoryForm form,
                                     BindingResult result,
                                     Model model) {
        if (!result.hasErrors()) {
            CategoryDTO category = categoryService.update(form.getId(), getCurrentUserId(), form.getName());

            model.addAttribute("updatedCategory", category);
            return "updatingCategory";
        }

        model.addAttribute("categoryForm", form);
        return "updateCategory";
    }

    @GetMapping("/delete-category")
    public String getDeleteCategory(Model model) {
        model.addAttribute("categoryForm", new CategoryForm());
        return "deleteCategory";
    }

    @PostMapping("/delete-category")
    public String postDeleteCategory(@ModelAttribute("categoryForm") @Valid CategoryForm form,
                                     BindingResult result,
                                     Model model) {
        if (!result.hasErrors()) {
            categoryService.remove(form.getId(), getCurrentUserId());
            return "success";
        }

        model.addAttribute("categoryForm", form);
        return "deleteCategory";
    }

    @GetMapping("/income")
    public String getIncomeReport(Model model) {
        model.addAttribute("reportForm", new ReportForm());
        return "incomeReport";
    }

    @PostMapping("/income")
    public String postIncomeReport(@ModelAttribute("reportForm") @Valid ReportForm form,
                                   BindingResult result,
                                   Model model) {
        if (!result.hasErrors()) {
            List<CategoryReportDTO> reports = categoryService.incomeReport(getCurrentUserId(), form.getBeginDate(), form.getEndDate());

            model.addAttribute("incomeReports", reports);
            return "displayIncomeReport";
        }

        model.addAttribute("reportForm", form);
        return "incomeReport";
    }

    @GetMapping("/outcome")
    public String getOutcomeReport(Model model) {
        model.addAttribute("reportForm", new ReportForm());
        return "outcomeReport";
    }

    @PostMapping("/outcome")
    public String postOutcomeReport(@ModelAttribute("reportForm") @Valid ReportForm form,
                                    BindingResult result,
                                    Model model) {
        if (!result.hasErrors()) {
            List<CategoryReportDTO> reports = categoryService.outcomeReport(getCurrentUserId(), form.getBeginDate(), form.getEndDate());

            model.addAttribute("outcomeReports", reports);
            return "displayOutcomeReport";
        }

        model.addAttribute("reportForm", form);
        return "outcomeReport";
    }
}

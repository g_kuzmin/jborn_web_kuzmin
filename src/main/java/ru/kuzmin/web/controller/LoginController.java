package ru.kuzmin.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.kuzmin.api.controller.BaseController;
import ru.kuzmin.entity.User;
import ru.kuzmin.service.AuthService;
import ru.kuzmin.service.UserDTO;
import ru.kuzmin.web.form.LoginForm;

import javax.validation.Valid;

@RequiredArgsConstructor
@Controller
public class LoginController extends BaseController {
    private final AuthService authService;

    @GetMapping("/")
    public String index(Model model) {
        User user = authService.getCurrentUser(getCurrentUserId());

        model.addAttribute("id", getCurrentUserId())
             .addAttribute("name", user.getEmail());

        return "personal-area";
    }

    @GetMapping("/login-form")
    public String getLogin() {
        return "login-form";
    }

    @GetMapping("/register")
    public String getRegister(Model model) {
        model.addAttribute("loginForm", new LoginForm());
        return "register";
    }

    @PostMapping("/register")
    public String postRegister(@ModelAttribute("loginForm") @Valid LoginForm form,
                            BindingResult result,
                            Model model) {
        if (!result.hasErrors()) {
            UserDTO user = authService.registration(form.getEmail(), form.getPassword());

            return "redirect:/";
        }
        model.addAttribute("loginForm", form);
        return "register";
    }
}

package ru.kuzmin.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.kuzmin.api.controller.BaseController;
import ru.kuzmin.service.AccountDTO;
import ru.kuzmin.service.AccountService;
import ru.kuzmin.web.form.AccountForm;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@Controller
public class AccountsController extends BaseController {
    private final AccountService accountService;

    @GetMapping("/display-accounts")
    public String getAccounts(Model model) {
        List<AccountDTO> accounts = accountService.findAll(getCurrentUserId());

        model.addAttribute("accounts", accounts);
        return "displayAccounts";
    }

    @GetMapping("/add-account")
    public String getAddAccount(Model model) {
        model.addAttribute("accountForm", new AccountForm());
        return "addAccount";
    }

    @PostMapping("/add-account")
    public String postAddAccount(@ModelAttribute("accountForm") @Valid AccountForm form,
                                BindingResult result,
                                Model model) {
        if (!result.hasErrors()) {
            AccountDTO account = accountService.create(getCurrentUserId(), form.getName(), form.getBalance());
            model.addAttribute("account", account);
            return "addingAccount";
        }

        model.addAttribute("accountForm", form);
        return "addAccount";
    }

    @GetMapping("/delete-account")
    public String getDeleteAccount(Model model) {
        model.addAttribute("accountForm", new AccountForm());
        return "deleteAccount";
    }

    @PostMapping("/delete-account")
    public String postDeleteAccount(@ModelAttribute("accountForm") @Valid AccountForm form,
                                    BindingResult result,
                                    Model model) {
        if (!result.hasErrors()) {
            accountService.remove(getCurrentUserId(), form.getId());
            return "success";
        }

        model.addAttribute("accountForm", form);
        return "deleteAccount";
    }
}

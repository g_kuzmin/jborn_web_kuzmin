package ru.kuzmin.web.form;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class AccountForm {
    private long id;

    @NotNull
    private String name;

    @NotNull
    private long balance;
    private long consumerId;
}

package ru.kuzmin.web.form;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@Accessors(chain = true)
public class LoginForm {
    @Email
    @NotEmpty
    private String email;

    @NotEmpty
    private String password;
}

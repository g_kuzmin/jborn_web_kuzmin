package ru.kuzmin.web.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
public class ReportForm {
    @NotNull
    private Date beginDate;

    @NotNull
    private Date endDate;
    private String name;
    private long sum;
}

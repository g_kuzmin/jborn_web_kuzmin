package ru.kuzmin.web.form;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CategoryForm {
    private long id;
    private long consumerId;

    @NotNull
    private String name;
}

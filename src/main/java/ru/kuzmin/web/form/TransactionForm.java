package ru.kuzmin.web.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.List;

@Data
public class TransactionForm {
    private long id;
    private Long fromAccountId;
    private Long toAccountId;

    @NotNull
    private long amount;

    @NotNull
    private Date createdDate;

    @NotNull
    private String categories;
}

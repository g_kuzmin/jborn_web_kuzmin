package ru.kuzmin.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kuzmin.api.converter.AccountDtoToResponseConverter;
import ru.kuzmin.api.json.AccountRequest;
import ru.kuzmin.api.json.AccountResponse;
import ru.kuzmin.service.AccountService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class AccountController extends BaseController {
    private final AccountService accountService;
    private final AccountDtoToResponseConverter converter;

    @GetMapping("/display-accounts")
    public ResponseEntity<List<AccountResponse>> getAccounts() {
        return ok(accountService.findAll(getCurrentUserId())
                        .stream()
                        .map(converter::convert)
                        .collect(Collectors.toList()));
    }

    @PostMapping("/add-account")
    public ResponseEntity<AccountResponse> addAccount(@RequestBody @Valid AccountRequest request) {
        return ok(converter.convert(accountService.create(
                getCurrentUserId(),
                request.getName(),
                request.getBalance())
        ));
    }

    @PostMapping("/delete-account")
    public ResponseEntity<String> deleteAccount(@RequestBody @Valid AccountRequest request) {
        accountService.remove(getCurrentUserId(), request.getId());
        return ok("Account deleted");
    }
}

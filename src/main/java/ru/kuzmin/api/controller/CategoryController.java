package ru.kuzmin.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kuzmin.api.converter.CategoryDtoToResponseConverter;
import ru.kuzmin.api.converter.CategoryReportDtoToResponseConverter;
import ru.kuzmin.api.json.CategoryReportRequest;
import ru.kuzmin.api.json.CategoryReportResponse;
import ru.kuzmin.api.json.CategoryRequest;
import ru.kuzmin.api.json.CategoryResponse;
import ru.kuzmin.service.CategoryService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class CategoryController extends BaseController {
    private final CategoryService categoryService;
    private final CategoryDtoToResponseConverter dtoConverter;
    private final CategoryReportDtoToResponseConverter reportConverter;

    @GetMapping("/display-categories")
    public ResponseEntity<List<CategoryResponse>> getCategories() {
        return ok(categoryService.findAll(getCurrentUserId())
                .stream()
                .map(dtoConverter::convert)
                .collect(Collectors.toList()));
    }

    @PostMapping("/add-category")
    public ResponseEntity<CategoryResponse> addCategory(@RequestBody @Valid CategoryRequest request) {
        return ok(dtoConverter.convert(categoryService.create(
                getCurrentUserId(), request.getName())
        ));
    }

    @PostMapping("/update-category")
    public ResponseEntity<CategoryResponse> updateCategory(@RequestBody @Valid CategoryRequest request) {
        return ok(dtoConverter.convert(categoryService.update(
                request.getId(),
                getCurrentUserId(),
                request.getName()
        )));
    }

    @PostMapping("/delete-category")
    public ResponseEntity<String> deleteCategory(@RequestBody @Valid CategoryRequest request) {
        categoryService.remove(request.getId(), getCurrentUserId());
        return ok("Category deleted");
    }

    @PostMapping("/income")
    public ResponseEntity<List<CategoryReportResponse>> incomeReport(@RequestBody @Valid CategoryReportRequest request) {
        return ok(categoryService.incomeReport(getCurrentUserId(), request.getBeginDate(), request.getEndDate())
                .stream()
                .map(reportConverter::convert)
                .collect(Collectors.toList()
                ));
    }

    @PostMapping("/outcome")
    public ResponseEntity<List<CategoryReportResponse>> outcomeReport(@RequestBody @Valid CategoryReportRequest request) {
        return ok(categoryService.outcomeReport(getCurrentUserId(), request.getBeginDate(), request.getEndDate())
                .stream()
                .map(reportConverter::convert)
                .collect(Collectors.toList()
                ));
    }
}

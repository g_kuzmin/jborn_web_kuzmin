package ru.kuzmin.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kuzmin.api.converter.TransactionDtoToResponseConverter;
import ru.kuzmin.api.json.TransactionRequest;
import ru.kuzmin.api.json.TransactionResponse;
import ru.kuzmin.service.TransactionService;

import javax.validation.Valid;
import java.sql.Date;

import static org.springframework.http.ResponseEntity.ok;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class TransactionController extends BaseController{
    private final TransactionService transactionService;
    private final TransactionDtoToResponseConverter converter;

    @PostMapping("/add-transaction")
    public ResponseEntity<TransactionResponse> addTransaction(@RequestBody @Valid TransactionRequest request) {
        return ok(converter.convert(transactionService.create(
                                getCurrentUserId(),
                                request.getFromAccountId(),
                                request.getToAccountId(),
                                request.getAmount(),
                                new Date(System.currentTimeMillis()),
                                request.getCategories()
                        )));
    }
}

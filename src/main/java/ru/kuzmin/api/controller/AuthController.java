package ru.kuzmin.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kuzmin.api.converter.UserDtoToResponseConverter;
import ru.kuzmin.api.json.AuthRequest;
import ru.kuzmin.api.json.AuthResponse;
import ru.kuzmin.service.AuthService;
import ru.kuzmin.service.UserDTO;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.ok;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class AuthController extends BaseController{
    private final AuthService authService;
    private final UserDtoToResponseConverter converter;

    @PostMapping("/register")
    public ResponseEntity<AuthResponse> register(@RequestBody @Valid AuthRequest request) {
        UserDTO user = authService.registration(
                request.getEmail(),
                request.getPassword()
        );

        return ok(converter.convert(user));
    }
}
package ru.kuzmin.api.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import ru.kuzmin.security.CustomUserDetails;

public class BaseController {
    protected Long getCurrentUserId() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        return user.getId();
    }
}

package ru.kuzmin.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CategoryResponse {
    private long id;
    private long consumerId;
    private String name;
}

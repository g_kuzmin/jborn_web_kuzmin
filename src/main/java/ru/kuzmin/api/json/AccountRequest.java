package ru.kuzmin.api.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AccountRequest {
    private long id;

    @NotNull
    private String name;

    @NotNull
    private long balance;
}

package ru.kuzmin.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CategoryReportResponse {
    private String name;
    private long sum;
}

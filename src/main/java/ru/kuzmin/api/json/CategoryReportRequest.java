package ru.kuzmin.api.json;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
public class CategoryReportRequest {
    @NotNull
    private Date beginDate;

    @NotNull
    private Date endDate;
}

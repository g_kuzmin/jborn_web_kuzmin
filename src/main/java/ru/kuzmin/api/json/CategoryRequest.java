package ru.kuzmin.api.json;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CategoryRequest {
    private long id;

    @NotNull
    private String name;
}

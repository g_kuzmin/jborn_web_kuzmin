package ru.kuzmin.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Date;

@Data
@AllArgsConstructor
public class TransactionResponse {
    private long id;
    private Long fromAccountId;
    private Long toAccountId;
    private long amount;
    private Date createdDate;
}

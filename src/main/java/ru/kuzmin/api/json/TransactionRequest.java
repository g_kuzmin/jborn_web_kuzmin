package ru.kuzmin.api.json;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.List;

@Data
public class TransactionRequest {
    private long fromAccountId;
    private long toAccountId;

    @NotNull
    private long amount;
    private Date createdDate;

    @NotNull
    private List<Long> categories;
}

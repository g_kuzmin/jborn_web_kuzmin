package ru.kuzmin.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.kuzmin.entity.Transaction;
import ru.kuzmin.service.TransactionDTO;

@Service
public class TransactionEntityToTransactionDtoConverter implements Converter<Transaction, TransactionDTO> {
    @Override
    public TransactionDTO convert(Transaction source) {
        return new TransactionDTO(
                source.getId(),
                source.getFromAccount() == null ? null : source.getFromAccount().getId(),
                source.getToAccount() == null ? null : source.getToAccount().getId(),
                source.getAmount(),
                source.getCreatedDate()
        );
    }
}

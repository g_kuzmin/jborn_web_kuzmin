package ru.kuzmin.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.kuzmin.entity.Account;
import ru.kuzmin.service.AccountDTO;

@Service
public class AccountEntityToAccountDtoConverter implements Converter<Account, AccountDTO> {
    @Override
    public AccountDTO convert(Account source) {
        return new AccountDTO(source.getId(), source.getName(),
                source.getBalance(), source.getUser().getId());
    }
}

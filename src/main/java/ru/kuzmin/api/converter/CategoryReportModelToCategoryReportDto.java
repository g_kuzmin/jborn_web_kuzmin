package ru.kuzmin.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.kuzmin.repository.CategoryReportModel;
import ru.kuzmin.service.CategoryReportDTO;

@Service
public class CategoryReportModelToCategoryReportDto implements Converter<CategoryReportModel, CategoryReportDTO> {
    @Override
    public CategoryReportDTO convert(CategoryReportModel source) {
        return new CategoryReportDTO(source.getName(), source.getSum());
    }
}

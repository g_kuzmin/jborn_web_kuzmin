package ru.kuzmin.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.kuzmin.api.json.AccountResponse;
import ru.kuzmin.entity.Account;
import ru.kuzmin.service.AccountDTO;

@Component
public class AccountDtoToResponseConverter implements Converter<AccountDTO, AccountResponse> {
    @Override
    public AccountResponse convert(AccountDTO account) {
        return new AccountResponse(account.getId(), account.getName(),
                account.getBalance(), account.getConsumerId());
    }
}

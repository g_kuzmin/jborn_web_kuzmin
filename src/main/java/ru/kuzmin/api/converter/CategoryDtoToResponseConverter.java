package ru.kuzmin.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.kuzmin.api.json.CategoryResponse;
import ru.kuzmin.entity.Category;
import ru.kuzmin.service.CategoryDTO;

@Component
public class CategoryDtoToResponseConverter implements Converter<CategoryDTO, CategoryResponse> {
    @Override
    public CategoryResponse convert(CategoryDTO category) {
        return new CategoryResponse(category.getId(), category.getConsumerId(), category.getName());
    }
}

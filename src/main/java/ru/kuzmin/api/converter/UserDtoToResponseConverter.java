package ru.kuzmin.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.kuzmin.api.json.AuthResponse;
import ru.kuzmin.service.UserDTO;

@Component
public class UserDtoToResponseConverter implements Converter<UserDTO, AuthResponse> {
    @Override
    public AuthResponse convert(UserDTO user) {
        return new AuthResponse(user.getId(), user.getEmail());
    }
}

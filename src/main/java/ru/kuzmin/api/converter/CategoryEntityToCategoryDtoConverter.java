package ru.kuzmin.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.kuzmin.entity.Category;
import ru.kuzmin.service.CategoryDTO;

@Service
public class CategoryEntityToCategoryDtoConverter implements Converter<Category, CategoryDTO> {
    @Override
    public CategoryDTO convert(Category source) {
        return new CategoryDTO(source.getId(), source.getUser().getId(), source.getName());
    }
}

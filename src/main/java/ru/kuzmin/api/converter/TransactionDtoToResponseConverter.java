package ru.kuzmin.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.kuzmin.api.json.TransactionResponse;
import ru.kuzmin.entity.Transaction;
import ru.kuzmin.service.TransactionDTO;

@Component
public class TransactionDtoToResponseConverter implements Converter<TransactionDTO, TransactionResponse> {
    @Override
    public TransactionResponse convert(TransactionDTO transaction) {
        return new TransactionResponse(
                transaction.getId(),
                transaction.getFromAccountId(),
                transaction.getToAccountId(),
                transaction.getAmount(),
                transaction.getCreatedDate()
        );
    }
}

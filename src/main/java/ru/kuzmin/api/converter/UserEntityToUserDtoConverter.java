package ru.kuzmin.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.kuzmin.entity.User;
import ru.kuzmin.service.UserDTO;

@Service
public class UserEntityToUserDtoConverter implements Converter<User, UserDTO> {
    @Override
    public UserDTO convert(User source) {
        return new UserDTO(source.getId(), source.getEmail());
    }
}

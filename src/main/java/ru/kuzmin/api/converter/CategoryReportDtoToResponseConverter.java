package ru.kuzmin.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.kuzmin.api.json.CategoryReportResponse;
import ru.kuzmin.service.CategoryReportDTO;

@Component
public class CategoryReportDtoToResponseConverter implements Converter<CategoryReportDTO, CategoryReportResponse> {
    @Override
    public CategoryReportResponse convert(CategoryReportDTO category) {
        return new CategoryReportResponse(category.getName(), category.getSum());
    }
}

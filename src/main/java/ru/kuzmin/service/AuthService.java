package ru.kuzmin.service;

import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kuzmin.entity.User;
import ru.kuzmin.exception.CustomException;
import ru.kuzmin.repository.UserRepository;
import ru.kuzmin.security.UserRole;

import javax.sql.DataSource;
import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class AuthService { ;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final Converter<User, UserDTO> userDtoConverter;

    public UserDTO auth(String email, String password) {
        User user = userRepository.findByEmail(email).orElseThrow(() -> new CustomException("Неверный логин!"));
        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new CustomException("Неверный пароль!");
        }

        return userDtoConverter.convert(user);
    }

    public UserDTO registration(String email, String password) {
        String hash = passwordEncoder.encode(password);
        Set<UserRole> roles = new HashSet<>();
        roles.add(UserRole.USER);

        return userDtoConverter.convert(userRepository.save(new User(email, hash).setRoles(roles)));
    }

    public User getCurrentUser(Long id) {
        return userRepository.getReferenceById(id);
    }
}

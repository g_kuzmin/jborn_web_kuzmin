package ru.kuzmin.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@Accessors(chain = true)
public class CategoryReportDTO {
    private String name;
    private long sum;
}

package ru.kuzmin.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@Accessors(chain = true)
public class AccountDTO {
    private long id;
    private String name;
    private long balance;
    private long consumerId;
}

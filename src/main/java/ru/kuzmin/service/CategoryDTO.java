package ru.kuzmin.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@Accessors(chain = true)
public class CategoryDTO {
    private long id;
    private long consumerId;
    private String name;
}

package ru.kuzmin.service;

import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kuzmin.entity.Account;
import ru.kuzmin.entity.Category;
import ru.kuzmin.entity.Transaction;
import ru.kuzmin.exception.CustomException;
import ru.kuzmin.repository.AccountRepository;
import ru.kuzmin.repository.CategoryRepository;
import ru.kuzmin.repository.TransactionRepository;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class TransactionService {
    private final TransactionRepository transactionRepository;
    private final CategoryRepository categoryRepository;
    private final AccountRepository accountRepository;
    private final Converter<Transaction, TransactionDTO> transactionDTOConverter;

    @Transactional
    public TransactionDTO create(long userId, long fromAccountId, long toAccountId, long amount, Date createdDate, List<Long> categories) {
        Account toAccount = accountRepository.findByIdAndUserId(toAccountId, userId).orElse(null);
        Account fromAccount = accountRepository.findByIdAndUserId(fromAccountId, userId).orElse(null);
        List<Category> categoryEntities = new ArrayList<>();
        for (Long categoryId : categories) {
            Category category = categoryRepository.findById(categoryId)
                    .orElseThrow(() -> new CustomException("id типа транзакций не был найден!"));

            categoryEntities.add(category);
        }

        Transaction transaction = new Transaction()
                .setToAccount(toAccount)
                .setFromAccount(fromAccount)
                .setAmount(amount)
                .setCreatedDate(createdDate)
                .setCategories(categoryEntities);

        if (fromAccountId == 0 && toAccountId != 0) {
            if (toAccount == null) {
                throw new CustomException("Нет аккаунтов с таким id");
            } else {
                accountRepository.save(toAccount.setBalance(toAccount.getBalance() + amount));

                return transactionDTOConverter.convert(transactionRepository.save(transaction));
            }
        } else if (toAccountId == 0 && fromAccountId != 0) {
            if (fromAccount == null) {
                throw new CustomException("Нет аккаунтов с таким id");
            } else {
                if (fromAccount.getBalance() < amount) {
                    throw new CustomException("Баланс счета недостаточен!");
                } else {
                    accountRepository.save(fromAccount.setBalance(fromAccount.getBalance() - amount));

                    return transactionDTOConverter.convert(transactionRepository.save(transaction));
                }
            }
        } else if (toAccountId != 0) {
            if (fromAccount == null || toAccount == null) {
                throw new CustomException("Нет аккаунтов с таким id");
            } else {
                if (fromAccount.getBalance() < amount) {
                    throw new CustomException("Баланс счета недостаточен!");
                } else {
                    accountRepository.save(fromAccount.setBalance(fromAccount.getBalance() - amount));

                    accountRepository.save(toAccount.setBalance(toAccount.getBalance() + amount));

                    return transactionDTOConverter.convert(transactionRepository.save(transaction));
                }
            }
        } else {
            throw new CustomException("Хотя бы один из id в транзакции должен быть не null");
        }
    }
}

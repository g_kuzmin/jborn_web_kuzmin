package ru.kuzmin.service;

import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.kuzmin.entity.Category;
import ru.kuzmin.entity.User;
import ru.kuzmin.exception.CustomException;
import ru.kuzmin.repository.CategoryReportModel;
import ru.kuzmin.repository.CategoryRepository;
import ru.kuzmin.repository.UserRepository;

import javax.sql.DataSource;
import java.sql.Date;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private final UserRepository userRepository;
    private final Converter<Category, CategoryDTO> categoryDtoConverter;

    private final Converter<CategoryReportModel, CategoryReportDTO> reportModelToCategoryReportDto;

    public List<CategoryDTO> findAll(long userId) {
        return categoryRepository.findCategoriesByUserId(userId).stream()
                .map(categoryDtoConverter::convert).collect(toList());
    }

    public CategoryDTO create(long userId, String name) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new CustomException("Пользователя с таким id не существует!"));

        return categoryDtoConverter.convert(categoryRepository.save(new Category().setName(name).setUser(user)));
    }

    public CategoryDTO update(long categoryId, long userId, String name) {
        Category category = categoryRepository.findByIdAndUserId(categoryId, userId)
                .orElseThrow(() -> new CustomException("id типа транзакций не был найден!"));

        return categoryDtoConverter.convert(categoryRepository.save(category.setName(name)));
    }

    public void remove(long categoryId, long userId) {
        Category category = categoryRepository.findByIdAndUserId(categoryId, userId)
                .orElseThrow(() -> new CustomException("id типа транзакций не был найден!"));

        categoryRepository.delete(category);
    }

    public List<CategoryReportDTO> incomeReport(long userId, Date beginDate, Date endDate) {
        return categoryRepository.incomeReport(userId, beginDate, endDate)
                .stream()
                .map(reportModelToCategoryReportDto::convert)
                .collect(toList()
                );
    }

    public List<CategoryReportDTO> outcomeReport(long userId, Date beginDate, Date endDate) {
        return categoryRepository.outcomeReport(userId, beginDate, endDate)
                .stream()
                .map(reportModelToCategoryReportDto::convert)
                .collect(toList()
                );
    }
}

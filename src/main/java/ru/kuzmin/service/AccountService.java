package ru.kuzmin.service;

import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kuzmin.entity.Account;
import ru.kuzmin.entity.User;
import ru.kuzmin.exception.CustomException;
import ru.kuzmin.repository.AccountRepository;
import ru.kuzmin.repository.UserRepository;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;
    private final Converter<Account, AccountDTO> accountDtoConverter;

    @Transactional
    public List<AccountDTO> findAll(long userId) {
        return accountRepository.findAccountsByUserId(userId).stream()
                .map(accountDtoConverter::convert).collect(toList());
    }

    @Transactional
    public AccountDTO create(long userId, String name, long balance) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new CustomException("Пользователя с таким id не существует!"));

        return accountDtoConverter.convert(
                accountRepository.save(
                        new Account()
                                .setName(name)
                                .setBalance(balance)
                                .setUser(user)
                ));
    }

    @Transactional
    public void remove(long userId, long accountId){
        Account account = accountRepository.findByIdAndUserId(accountId, userId)
                .orElseThrow(() -> new CustomException("Такого счета не существует!"));
        accountRepository.delete(account);
    }
}

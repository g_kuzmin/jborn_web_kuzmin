package ru.kuzmin.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

import java.sql.Date;

@Data
@AllArgsConstructor
@Accessors(chain = true)
public class TransactionDTO {
    private long id;
    private Long fromAccountId;
    private Long toAccountId;
    private long amount;
    private Date createdDate;
}

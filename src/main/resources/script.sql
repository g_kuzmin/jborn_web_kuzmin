create table consumer
(
    id       serial primary key,
    email    varchar(255) unique not null,
    password varchar(255) not null
);

create table user_role
(
    user_id int references consumer (id),
    role     varchar(255) not null
);

create table account
(
    id          serial primary key,
    name        varchar(255) not null,
    balance     int          not null,
    consumer_id int references consumer (id)
);

create table transaction
(
    id              serial primary key,
    from_account_id int references account(id) null,
    to_account_id  int references account(id) null,
    amount             int  not null,
    created_date       timestamp not null
);

create table category
(
    id          serial primary key,
    consumer_id int references consumer (id),
    name varchar(255)
);

create table category_to_transaction
(
    category_id    int references category (id),
    transaction_id int references transaction (id)
);

insert into consumer (email, password) values ('123@mail.ru', '$2a$10$Hlqyr2F.lCFAGT4ErtgAl.yQVpT2YBQXceVqBiPX0lodbZjzZPyFW'); --123
insert into consumer (email, password) values ('qwe@mail.ru', '$2a$10$F5l3t4hMB9Gl8Rb.XHe1behWp9gc1VbfdZ7hhLdkgdWPmxXGvTYEe'); --qwe

insert into user_role (user_id, role) values (1, 'USER');
insert into user_role (user_id, role) values (2, 'USER');

insert into account (name, balance, consumer_id) values ('bank account', 50000, 1);
insert into account (name, balance, consumer_id) values ('cash', 15500, 1);
insert into account (name, balance, consumer_id) values ('bank account', 37700, 2);

insert into transaction (from_account_id, to_account_id, amount, created_date) values (1, 2, 1000, '2021-10-30 15:55:00'); -- перевод между своими счетами
insert into transaction (from_account_id, to_account_id, amount, created_date) values (1, null, 200, '2022-08-18 10:03:00'); -- трата денег
insert into transaction (from_account_id, to_account_id, amount, created_date) values (null, 3, 19650, '2022-07-29 23:59:00'); -- поступление денег
insert into transaction (from_account_id, to_account_id, amount, created_date) values (3, null, 300, '2022-08-18 10:03:00'); -- трата денег

insert into category (consumer_id, name) values (1, 'money transfer');
insert into category (consumer_id, name) values (1, 'spending');
insert into category (consumer_id, name) values (2, 'payment');
insert into category (consumer_id, name) values (2, 'salary');

insert into category_to_transaction (category_id, transaction_id) values (1, 1);
insert into category_to_transaction (category_id, transaction_id) values (2, 2);
insert into category_to_transaction (category_id, transaction_id) values (4, 3);
insert into category_to_transaction (category_id, transaction_id) values (3, 4);


select *  -- Вывести все счета пользователя
from account
where consumer_id = 1;

select t.*  -- Вывести все транзакции пользователя за вчера
from transaction t
         join category_to_transaction ctt on t.id = ctt.transaction_id
         join category c on c.id = ctt.category_id
where consumer_id = 1
  and (t.created_date between 'yesterday'::timestamp and 'today'::timestamp);

select consumer_id, sum(balance)  -- Вывести сумму всех остатков на счетах по всем пользователям
from account
group by consumer_id
order by consumer_id;
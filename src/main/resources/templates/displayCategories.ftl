<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Categories</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        table {
            width: 1000px;
            border: 2px solid black;
            margin: auto;
            border-collapse: collapse;
        }
        TD, TH {
            padding: 3px;
            border: 1px solid black;
            text-align: center;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<h2 align="center">Categories</h2>
<table>
    <tr>
        <th>id</th>
        <th>name</th>
        <th>consumerId</th>
    </tr>
    <#list categories as category>
        <tr>
            <td>${category.id}</td>
            <td>${category.name}</td>
            <td>${category.consumerId}</td>
        </tr>
    <#else>
        <div class="container text-center">
            <h2>No categories found</h2>
        </div>
    </#list>
</table>
<div class="container text-center">
    <a href="/">Main page</a>
</div>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container text-center">
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <h2>Sign in to system</h2>
            <form action="/login" method="post">
                <div class="mb-3">
                    <label for="exampleInputEmail" class="form-label">Email address</label>
                    <input name="email" class="form-control" id="exampleInputEmail" placeholder="Enter email" type="text"/>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword" class="form-label">Password</label>
                    <input name="password" class="form-control" id="exampleInputPassword" placeholder="Enter password" type="password"/>
                </div>
                <button type="submit" class="btn btn-primary">Sign in</button><br>
                <a href="/register">Sign up</a>
            </form>
        </div>
        <div class="col"></div>
    </div>
</div>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
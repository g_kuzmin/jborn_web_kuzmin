<#import "/spring.ftl" as spring />

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Income report</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container text-center">
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <h2>Report</h2>
            <form action="/income" method="post">
                <div class="mb-3">
                    <label>BeginDate</label>
                    <@spring.formInput "reportForm.beginDate" "class=\"form-control\" id=\"exampleInputEmail\" placeholder=\"Enter beginDate\"" "date"/>
                    <@spring.showErrors "<br>" />
                </div>
                <div class="mb-3">
                    <label>EndDate</label>
                    <@spring.formInput "reportForm.endDate" "class=\"form-control\" id=\"exampleInputEmail\" placeholder=\"Enter endDate\"" "date"/>
                    <@spring.showErrors "<br>" />
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col"></div>
    </div>
</div>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
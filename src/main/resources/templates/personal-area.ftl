<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hello</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container text-center">
    <h2>UserId: ${id} </h2><br>
    <h2>Hello, ${name}! </h2><br>
    <h2> You successfully signed in</h2><br>
    <a href="/login">Sign in</a><br>
    <a href="/register">Sign up</a><br>
    <a href="/display-accounts">Display accounts</a><br>
    <a href="/add-account">Add account</a><br>
    <a href="/delete-account">Delete account</a><br>
    <a href="/display-categories">Display categories</a><br>
    <a href="/add-category">Add category</a><br>
    <a href="/update-category">Update category</a><br>
    <a href="/delete-category">Delete category</a><br>
    <a href="/income">Income report</a><br>
    <a href="/outcome">Outcome report</a><br>
    <a href="/add-transaction">Add transaction</a><br><br>
    <a href="/logout">Logout</a><br>
</div>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
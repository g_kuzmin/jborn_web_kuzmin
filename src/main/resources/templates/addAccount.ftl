<#import "/spring.ftl" as spring />

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add account</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container text-center">
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <h2>Add account</h2>
            <form action="/add-account" method="post">
                <div class="mb-3">
                    <label>Account name</label>
                    <@spring.formInput "accountForm.name" "class=\"form-control\" id=\"exampleInputEmail\" placeholder=\"Enter name\"" "text"/>
                    <@spring.showErrors "<br>" />
                </div>
                <div class="mb-3">
                    <label>Balance</label>
                    <@spring.formInput "accountForm.balance" "class=\"form-control\" id=\"exampleInputPassword\" placeholder=\"Enter balance\"" "number"/>
                    <@spring.showErrors "<br>" />
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col"></div>
    </div>
</div>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>